This module accompanies the book
[Subsurface Environmental Modelling Between Science and Policy](https://doi.org/10.1007/978-3-030-51178-4)
by Dirk Scheer, Holger Class, and Bernd Flemisch.
It originated from
[dumux-lecture](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture)
and is intended mostly for reproducing the results and figures employed in the book.
If you would like to develop your own applications based on the investigated scenarios,
we recommend that you profit from ongoing improvements by cloning a recent version of
[dumux-lecture](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture).

## Installation with Docker 

Create a new folder in your favourite location and change into it

```bash
mkdir dumux
cd dumux
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/scheer2020/-/raw/master/docker/pubtable_scheer2020
```
Open the Docker Container
````bash
bash pubtable_scheer2020 open
````

## Installation

Building from source requires a C++17 conforming compiler, CMake and pkg-config.
Clone this repository in your favourite location
```bash
git clone https://git.iws.uni-stuttgart.de/dumux-pub/scheer2020.git
```

Download the dependencies
```bash
./scheer2020/installscheer2020.sh
```

Configure and build libraries
```bash
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
```

## Reproducing the results

You can find the example applications of the book in the following subfolders:

| Subfolder | Book Section and Example Application | Reproduction Script |
|-----------|--------------------------------------|---------------------|
| [lecture/mm/co2plume](lecture/mm/co2plume) | 6.4.1 CO2 Plume Shape Development | [reproduce_figure_6_6.sh](lecture/mm/co2plume/reproduce_figure_6_6.sh) |
| [lecture/mm/convectivemixing](lecture/mm/convectivemixing) | 6.4.2 Convective Mixing | [reproduce_figure_6_8.sh](lecture/mm/convectivemixing/reproduce_figure_6_8.sh) |
| [lecture/mm/fractures](lecture/mm/fractures) | 7.4 Hydraulic Fracturing | [reproduce_figure_7_7.sh](lecture/mm/fractures/reproduce_figure_7_7.sh) |
| [lecture/mm/heatpipe](lecture/mm/heatpipe) | 8.4 Heat-Pipe Effect | [reproduce_figure_8_4.sh](lecture/mm/heatpipe/reproduce_figure_8_4.sh) |
| [lecture/mm/remediationscenarios](lecture/mm/remediationscenarios) | 9.2.4 Remediation of Contaminated Soils | [reproduce_figure_9_5.sh](lecture/mm/remediationscenarios/reproduce_figure_9_5.sh) |

In order to reproduce a figure, change to the corresponding build folder and
execute the script. For example, for the first application:
```bash
cd ./scheer2020/build-cmake/lecture/mm/co2plume/
./reproduce_figure_6_6.sh
```
This will build the executable, run it with the required parameters and open Paraview
with a dedicated state file that is close to the presentation of the figure in the book.

## Dependencies on other DUNE modules

| module | branch |
|:-------|:-------|
| dune-\{core, uggrid, alugrid, foamgrid\} | releases/2.7 |
| dumux | releases/3.2 |
