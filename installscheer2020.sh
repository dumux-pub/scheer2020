git clone -b releases/2.7 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.7 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.7 https://gitlab.dune-project.org/staging/dune-uggrid.git
git clone -b releases/2.7 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.7 https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone -b releases/2.7 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.7 https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone -b releases/2.7 https://gitlab.dune-project.org/extensions/dune-foamgrid.git

git clone -b releases/3.2 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
