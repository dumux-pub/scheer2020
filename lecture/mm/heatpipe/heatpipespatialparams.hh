// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_SPATIAL_PARAMS_HH
#define DUMUX_HEATPIPE_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv.hh>

#include "krpcheatpipe.hh"

namespace Dumux
{

template<class FVGridGeometry, class Scalar>
class HeatPipeSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, HeatPipeSpatialParams<FVGridGeometry, Scalar>>

{
    using ThisType = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    using PermeabilityType = Scalar;
    using MaterialLaw = KrPcHeatpipe<Scalar>;
    using MaterialLawParams = typename MaterialLaw::Params;

    HeatPipeSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        permeability_ = getParam<Scalar>("Problem.Permeability");
        porosity_ = 0.4;

        materialParams_.setSwr(0.15);
        materialParams_.setSnr(0.0);
        materialParams_.setP0(std::pow((porosity_/permeability_), 0.5));
    }

    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        return permeability_;
    }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        return porosity_;
    }

    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        return materialParams_;
    }

    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        return FluidSystem::phase0Idx;
    }

private:
    PermeabilityType permeability_;
    Scalar porosity_;
    MaterialLawParams materialParams_;
};

}

#endif // DUMUX_HEATPIPE_SPATIAL_PARAMS_HH
