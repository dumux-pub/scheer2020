#!/bin/bash

# check if the script is executed from the correct folder
if [ "$(basename $PWD)" != "heatpipe" ]; then
    echo "You have to run the script from the heatpipe folder!"
    exit -1
fi

# compile and run
make heatpipe
./heatpipe -Problem.Name heatpipe_1.0 -Problem.MultiplierPC 1.0
./heatpipe -Problem.Name heatpipe_0.5 -Problem.MultiplierPC 0.5

if [ -x "$(command -v paraview)" ]; then
    paraview --state=heatpipe.pvsm &
else
    if [ -d "/dumux/shared" ]; then
        cp heatpipe_* /dumux/shared/.
        cp heatpipe.pvsm /dumux/shared/.
        echo "Copied the result files to the folder /dumux/shared which corresponds to the"
        echo "folder on the host from where the Docker container has been started."
        echo "Go to this folder in the host system and execute \"paraview --state=heatpipe.pvsm\"."
    else
        echo "Paraview could not be found on your system. Try to execute it manually and"
        echo "pass the state file heatpipe.pvsm."
    fi
fi
