// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_KR_PC_HEATPIPE_HH
#define DUMUX_KR_PC_HEATPIPE_HH

#include <dumux/common/parameters.hh>
#include "krpcheatpipeparams.hh"

#include <algorithm>

namespace Dumux
{
/*!
 *
 * \brief Implementation of the capillary pressure <-> saturation
 *        and relative permeability <-> saturation relation for the heatpipe
 *        problem. rel-perm is based on the model of Fatt and Klikoff,
 *        cap-press based on the function of Leverett.
 *
 */
template <class ScalarT, class ParamsT = KrPcHeatpipeParams<ScalarT> >
class KrPcHeatpipe
{
public:
    using Params = ParamsT;
    using Scalar = ScalarT;

    /*!
     * \brief The capillary pressure-saturation curve according to Leverett.
     *
     */
    static Scalar pc(const Params &params, Scalar sw)
    {
        if(sw<0.) sw=0.;
         /* effective values */
        Scalar Swe = (sw-params.swr())/(1-params.snr()-params.swr());
        if(Swe<0) Swe=0.;
        if(Swe>1.0) Swe=1.0;
        Scalar f = 1.417*(1-Swe)-2.120*std::pow((1-Swe),2)+1.263*std::pow((1-Swe),3);
        Scalar sigma=0.0588;
        Scalar value= params.p0()*sigma*f;
        /* regularization for small saturations */
        if(sw<params.swr())
        { Scalar a = -0.966*params.p0()*sigma/(4.*std::pow(params.swr(),3));
          Scalar c = 0.56*params.p0()*sigma - a*std::pow(params.swr(),4);
          value = a*std::pow(sw,4)+c;
        }

        Scalar MODIFY_ME_CAPILLARY_PRESSURE = getParam<Scalar>("Problem.MultiplierPC");

        return(value * MODIFY_ME_CAPILLARY_PRESSURE);
    }

    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium according to the Fatt-Klikoff
     *        parameterization.
     */
    static Scalar krw(const Params &params, Scalar sw)
    {
         /*** effective saturation ***/
         Scalar Se = (sw-params.swr())/(1-params.snr()-params.swr());
         /* effective Saturation Se has to be between 0 and 1! */
         if (Se>1.)  Se=1.;
         if (Se<0) Se=0;
         /* compute and return value */
         return std::pow(Se,3);
    };

    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium according to the Fatt-Klikoff
     *        parameterization.
     */
    static Scalar krn(const Params &params, Scalar sw)
    {
         /*** effective saturation ***/
         Scalar Se = (sw-params.swr())/(1-params.snr()-params.swr());
         /* effective Saturation Se has to be between 0 and 1! */
         if (Se>1.)  Se=1.;
         if (Se<0) Se=0;
         /* compute and return value */
         return std::pow(1.-Se,3);
    };


};
}

#endif // DUMUX_KR_PC_HEATPIPE_HH
