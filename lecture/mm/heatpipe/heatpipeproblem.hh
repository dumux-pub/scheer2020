// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_HEATPIPE_PROBLEM_HH
#define DUMUX_HEATPIPE_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>

#include <dumux/material/fluidsystems/h2oair.hh>

#include <dumux/porousmediumflow/2p2c/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "heatpipespatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class HeatPipeProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct HeatPipeTypeTag { using InheritsFrom = std::tuple<TwoPTwoCNI, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::HeatPipeTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::HeatPipeTypeTag> { using type = HeatPipeProblem<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::HeatPipeTypeTag> { using type = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>; };

// pn-sw formulation
template<class TypeTag>
struct Formulation<TypeTag, TTag::HeatPipeTypeTag> { static constexpr auto value = TwoPFormulation::p1s0; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::HeatPipeTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using type = HeatPipeSpatialParams<FVGridGeometry, Scalar>;
};

}

template <class TypeTag >
class HeatPipeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem =GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;

public:
    HeatPipeProblem(std::shared_ptr<const FVGridGeometry> fVGridGeometry)
    : ParentType(fVGridGeometry)
    {
        FluidSystem::init();
    }

    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        if(globalPos[0] < eps_)
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        // left boundary: two-phase conditions, atmospheric pressure, almost full water saturation
        // 68.6 degree C;
        // we could use another phase presence (onlyWater) on the left, here simplified for better
        // convergence behavior
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 0.99;
        values[Indices::temperatureIdx] = 341.75;
        values.setState(Indices::bothPhases);

        return values;
    }

    NumEqVector neumannAtPos( const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        // negative values for injection
        // right boundary: constant heat-only flux, like from a cooktop
        if (globalPos[0] > (this->gridGeometry().bBoxMax()[0] - eps_))
        {
            values[Indices::energyEqIdx] = getParam<Scalar>("Problem.HeatFlux");
        }

        return values;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
      return initial_(globalPos);
    }

private:
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        // since we are interested in a steady-state,
        // the initial conditions serve mainly to illustrate the way towards the
        // steady state, but they may be changed as desired
        values[Indices::pressureIdx] = 1.013e5;
        values[Indices::switchIdx] = 0.5;
        values[Indices::temperatureIdx] = 343.15;
        values.setState(Indices::bothPhases);

        return values;
    }

    static constexpr Scalar eps_ = 1e-6;
};

} // namespace Dumux

#endif // DUMUX_HEATPIPE_PROBLEM_HH
