// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \brief Specification of the material parameters
 *       for the Brooks Corey constitutive relations.
 */
#ifndef DUMUX_KR_PC_HEATPIPE_PARAMS_HH
#define DUMUX_KR_PC_HEATPIPE_PARAMS_HH

#include <dumux/common/valgrind.hh>

namespace Dumux
{

/*!
 * \brief Specification of the material parameters
 *       for the kr-pc constitutive relations for the heatpipe problem.
 *
 */
template <class ScalarT>
class KrPcHeatpipeParams
{
public:
    typedef ScalarT Scalar;

    KrPcHeatpipeParams()
    {
        Valgrind::SetUndefined(*this);
    }

    KrPcHeatpipeParams(Scalar swr, Scalar snr)
        : swr_(swr), snr_(snr)
    {
    }

    /*!
     * \brief Returns the entry pressure [Pa]
     */
    Scalar swr() const
    { return swr_; }

    /*!
     * \brief Set the entry pressure [Pa]
     */
    void setSwr(Scalar v)
    { swr_ = v; }


    /*!
     * \brief Returns the lambda shape parameter
     */
    Scalar snr() const
    { return snr_; }

    /*!
     * \brief Set the lambda shape parameter
     */
    void setSnr(Scalar v)
    { snr_ = v; }

    /*!
     * \brief Returns the lambda shape parameter
     */
    Scalar p0() const
    { return p0_; }

    /*!
     * \brief Set the lambda shape parameter
     */
    void setP0(Scalar v)
    { p0_ = v; }

private:
    Scalar swr_;
    Scalar snr_;
    Scalar p0_;
};
} // namespace Dumux

#endif
