#!/bin/bash

# check if the script is executed from the correct folder
if [ "$(basename $PWD)" != "remediationscenarios" ]; then
    echo "You have to run the script from the remediationscenarios folder!"
    exit -1
fi

# compile and run
make remediationscenariosexercise
./remediationscenariosexercise -TimeLoop.TEnd 4320

if [ -x "$(command -v paraview)" ]; then
    paraview --state=remediation.pvsm &
else
    if [ -d "/dumux/shared" ]; then
        cp remediation-steam* /dumux/shared/.
        cp remediation.pvsm /dumux/shared/.
        echo "Copied the result files to the folder /dumux/shared which corresponds to the"
        echo "folder on the host from where the Docker container has been started."
        echo "Go to this folder in the host system and execute \"paraview --state=remediation.pvsm\"."
    else
        echo "Paraview could not be found on your system. Try to execute it manually and"
        echo "pass the state file remediation.pvsm."
    fi
fi
