#!/bin/bash

if [ "$(basename $PWD)" != "convectivemixing" ]; then
    echo "You have to run the script from the convectivemixing folder!"
    exit -1
fi

make convmixexercise
./convmixexercise -Grid.Cells "30 30" -TimeLoop.TEnd 5e8 -Problem.Name convmix_30x30
./convmixexercise -Grid.Cells "100 100" -TimeLoop.TEnd 5e8 -Problem.Name convmix_100x100

if [ -x "$(command -v paraview)" ]; then
    paraview --state=convectivemixing.pvsm &
else
    if [ -d "/dumux/shared" ]; then
        cp convmix_* /dumux/shared/.
        cp convectivemixing.pvsm /dumux/shared/.
        echo "Copied the result files to the folder /dumux/shared which corresponds to the"
        echo "folder on the host from where the Docker container has been started."
        echo "Go to this folder in the host system and execute \"paraview --state=convectivemixing.pvsm\"."
    else
        echo "Paraview could not be found on your system. Try to execute it manually and"
        echo "pass the state file convectivemixing.pvsm."
    fi
fi

