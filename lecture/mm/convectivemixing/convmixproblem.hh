// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Definition of a problem, for the 1pnc problem:
 * Component transport of nitrogen dissolved in the water phase.
 */
#ifndef DUMUX_CONVMIXING_PROBLEM_HH
#define DUMUX_CONVMIXING_PROBLEM_HH

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/brineco2.hh>
#include <dumux/material/binarycoefficients/brine_co2.hh>
#include <lecture/common/co2tablesbenchmark3.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

#include <dumux/material/fluidsystems/1padapter.hh>

#include "convmixspatialparams.hh"

namespace Dumux {

template <class TypeTag>
class ConvmixProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct ConvmixTypeTag { using InheritsFrom = std::tuple<OnePNC>; };
struct ConvmixBoxTypeTag { using InheritsFrom = std::tuple<ConvmixTypeTag, BoxModel>; };
} // end namespace TTag

template<class TypeTag>
struct Grid<TypeTag, TTag::ConvmixTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ConvmixTypeTag> { using type = ConvmixProblem<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ConvmixTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using BrineCO2 = FluidSystems::BrineCO2<Scalar, CO2TablesBenchmarkThree::CO2Tables,
                           Components::TabulatedComponent<Components::H2O<GetPropType<TypeTag, Properties::Scalar>>>,
                           FluidSystems::BrineCO2DefaultPolicy</*constantSalinity=*/true, /*simplified=*/true>>;
    using type = FluidSystems::OnePAdapter<BrineCO2, BrineCO2::liquidPhaseIdx>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ConvmixTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = ConvmixSpatialParams<FVGridGeometry, Scalar>;
};

template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::ConvmixTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = DiffusivityConstantTortuosity<Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::ConvmixTypeTag> { static constexpr bool value = true; };
}

/*!
 * \brief Definition of a problem, for the 1pnc problem:
 *
 * This problem uses the \ref OnePNCModel model.
 *
 */
template <class TypeTag>
class ConvmixProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using CO2 = Components::CO2<Scalar, CO2TablesBenchmarkThree::CO2Tables>;
    using Brine_CO2 = BinaryCoeff::Brine_CO2<Scalar, CO2TablesBenchmarkThree::CO2Tables>;

    // copy some indices for convenience
    enum
    {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,

        // component indices
        BrineIdx = FluidSystem::MultiPhaseFluidSystem::comp0Idx,
        CO2Idx = FluidSystem::MultiPhaseFluidSystem::CO2Idx,

        // equation indices
        conti0EqIdx = Indices::conti0EqIdx,
        contiCO2EqIdx = conti0EqIdx + CO2Idx
    };

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    static const bool isBox = GetPropType<TypeTag, Properties::GridGeometry>::discMethod == DiscretizationMethod::box;

    static const int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;

public:
    ConvmixProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        depthBOR_ = getParam<Scalar>("Problem.DepthBOR");
        name_ = getParam<std::string>("Problem.Name");

        //initialize fluid system
        FluidSystem::init();

        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions"<<std::endl;
        else
            std::cout<<"problem uses mass fractions"<<std::endl;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the temperature within the domain [K].
     *
     * This problem assumes a temperature of 20 degrees Celsius.
     */
    Scalar temperatureAtPos(const GlobalPosition &globalPos) const
    {
        return (283.15 + (depthBOR_ - globalPos[1])*0.032); // geothermal temperatur in [K]
                                                            // assuming 10°C at surface and a
                                                            // geothermal gradient of 0.032 K/m
    };

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        values.setAllNeumann();

        if(globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
            values.setDirichlet(CO2Idx, contiCO2EqIdx);
        if(globalPos[1] < eps_)
            values.setAllDirichlet();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values = initial_(globalPos);

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The neumann values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub control volume face
     *
     * For this method, the \a values parameter stores the flux
     * in normal direction of each phase. Negative values mean influx.
     * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        // no-flow everywhere
        return flux;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a priVars parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }

    // \}

private:
    // the internal method for the initial condition
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars;

        const Scalar temp = temperatureAtPos(globalPos);
        const Scalar salinity = getParam<Scalar>("Brine.Salinity");

        priVars[pressureIdx] = 1.013e5 + (depthBOR_ - globalPos[1]) * 1100 * 9.81;  // hydrostatic pressure distribution
        priVars[CO2Idx] = 0.;  // initial condition for the CO2 massfraction

        Scalar moleFracCO2, xgH2O;
        Brine_CO2::calculateMoleFractions(temp,
                                          priVars[pressureIdx],
                                          salinity,
                                          /*knownPhaseIdx=*/-1,
                                          moleFracCO2,
                                          xgH2O);

        if(globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
        {
            priVars[CO2Idx] = moleFracCO2;   // massfraction at top boundary [-]
        }

        return priVars;
    }
        static constexpr Scalar eps_ = 1e-6;

        Scalar depthBOR_; // bottom of reservoir [m]
        std::string name_ ;
    };

} //end namespace Dumux

#endif
