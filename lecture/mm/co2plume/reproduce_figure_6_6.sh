#!/bin/bash

if [ "$(basename $PWD)" != "co2plume" ]; then
    echo "You have to run the script from the co2plume folder!"
    exit -1
fi

make co2plumeshapeexercise
./co2plumeshapeexercise -BoundaryConditions.InjectionRate 0.001 -Problem.Name co2plume_1em3 -TimeLoop.EpisodeLength 1e7
./co2plumeshapeexercise -BoundaryConditions.InjectionRate 0.01 -Problem.Name co2plume_1em2 -TimeLoop.EpisodeLength 1e6

if [ -x "$(command -v paraview)" ]; then
    paraview --state=co2plume.pvsm &
else
    if [ -d "/dumux/shared" ]; then
        cp co2plume_1em* /dumux/shared/.
        cp co2plume.pvsm /dumux/shared/.
        echo "Copied the result files to the folder /dumux/shared which corresponds to the"
        echo "folder on the host from where the Docker container has been started."
        echo "Go to this folder in the host system and execute \"paraview --state=co2plume.pvsm\"."
    else
        echo "Paraview could not be found on your system. Try to execute it manually and"
        echo "pass the state file co2plume.pvsm."
    fi
fi
