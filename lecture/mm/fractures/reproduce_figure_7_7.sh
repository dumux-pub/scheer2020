#!/bin/bash

# check if the script is executed from the correct folder
if [ "$(basename $PWD)" != "fractures" ]; then
    echo "You have to run the script from the fractures folder!"
    exit -1
fi

# compile and run
make fracture_exercise
./fracture_exercise

# possibly fix file names in state files
# this will replace the link to the repository source file by an actual file
FIRSTMATRIXFILENAME="$(grep '14400' matrix.pvd | cut -d '"' -f10)"
if [ ! -z "$FIRSTMATRIXFILENAME" ]
then
    sed -i "s/matrix-001[0-9][0-9].vtu/$FIRSTMATRIXFILENAME/g" fractures_7_7.pvsm
    sed -i "s/matrix-001[0-9][0-9].vtu/$FIRSTMATRIXFILENAME/g" fractures_7_8.pvsm
fi

FIRSTFRACTUREFILENAME="$(grep '14400' fractures.pvd | cut -d '"' -f10)"
if [ ! -z "$FIRSTFRACTUREFILENAME" ]
then
    sed -i "s/fractures-001[0-9][0-9].vtp/$FIRSTFRACTUREFILENAME/g" fractures_7_7.pvsm
    sed -i "s/fractures-001[0-9][0-9].vtp/$FIRSTFRACTUREFILENAME/g" fractures_7_8.pvsm
fi

LASTMATRIXFILENAME="$(grep '100000' matrix.pvd | cut -d '"' -f10)"
if [ ! -z "$LASTMATRIXFILENAME" ]
then
    sed -i "s/matrix-00[5-6][0-9][0-9].vtu/$LASTMATRIXFILENAME/g" fractures_7_7.pvsm
    sed -i "s/matrix-00[5-6][0-9][0-9].vtu/$LASTMATRIXFILENAME/g" fractures_7_8.pvsm
fi

LASTFRACTUREFILENAME="$(grep '100000' fractures.pvd | cut -d '"' -f10)"
if [ ! -z "$LASTFRACTUREFILENAME" ]
then
    sed -i "s/fractures-00[5-6][0-9][0-9].vtp/$LASTFRACTUREFILENAME/g" fractures_7_7.pvsm
    sed -i "s/fractures-00[5-6][0-9][0-9].vtp/$LASTFRACTUREFILENAME/g" fractures_7_8.pvsm
fi

if [ -x "$(command -v paraview)" ]; then
    paraview --state=fractures_7_7.pvsm &
    paraview --state=fractures_7_8.pvsm &
else
    if [ -d "/dumux/shared" ]; then
        cp matrix-* /dumux/shared/.
        cp matrix.pvd /dumux/shared/.
        cp fractures-* /dumux/shared/.
        cp fractures.pvd /dumux/shared/.
        cp fractures_7_* /dumux/shared/.
        echo "Copied the result files to the folder /dumux/shared which corresponds to the"
        echo "folder on the host from where the Docker container has been started."
        echo "Go to this folder in the host system and execute \"paraview --state=fractures_7_7.pvsm\""
        echo "and/or \"paraview --state=fractures_7_8.pvsm\"."
    else
        echo "Paraview could not be found on your system. Try to execute it manually and"
        echo "pass the state file fractures_7_7.pvsm or fractures_7_8.pvsm."
    fi
fi
