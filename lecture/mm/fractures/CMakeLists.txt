dune_symlink_to_source_files(FILES "grids" "fracture_exercise.input" "plot.p" "fractures_7_7.pvsm" "fractures_7_8.pvsm" "reproduce_figure_7_7.sh")

# test for the exercise
dune_add_test(NAME fracture_exercise
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              SOURCES fractures.cc
              COMMAND ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
              CMD_ARGS --script fuzzy
                 --files ${CMAKE_SOURCE_DIR}/lecture/references/fractures-matrix-reference.vtu
                         ${CMAKE_CURRENT_BINARY_DIR}/matrix-00018.vtu
                         ${CMAKE_SOURCE_DIR}/lecture/references/fractures-fractures-reference.vtp
                         ${CMAKE_CURRENT_BINARY_DIR}/fractures-00018.vtp
                 --command "${CMAKE_CURRENT_BINARY_DIR}/fracture_exercise -TimeLoop.TEnd 50")

set(CMAKE_BUILD_TYPE Release)

#install sources
install(FILES
fractures.cc
matrixproblem.hh
fractureproblem.hh
matrixspatialparams.hh
fracturespatialparams.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lecture/mm/fractures)
