// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH

// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// we want to simulate methan gas transport in a water-saturated medium
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>

// We are using the framework for models that consider coupling
// across the element facets of the bulk domain. This has some
// properties defined, which we have to inherit here. In this
// exercise we want to use a cell-centered finite volume scheme
// with tpfa.
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {
template<class TypeTag, class MyTypeTag>
struct GridData { using type = UndefinedProperty; }; // forward declaration of property

// create the type tag node for the matrix sub-problem
// We need to put the facet-coupling type tag after the physics-related type tag
// because it overwrites some of the properties inherited from "TwoP". This is
// necessary because we need e.g. a modified implementation of darcys law that
// evaluates the coupling conditions on faces that coincide with the fractures.
// Create new type tags
namespace TTag {
struct MatrixProblemTypeTag { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TwoP>; };
} // end namespace TTag
// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::MatrixProblemTypeTag> { using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::MatrixProblemTypeTag> { using type = MatrixSubProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MatrixProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = MatrixSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MatrixProblemTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2O = Dumux::Components::H2O<Scalar>;
    using TabH2O = Dumux::Components::TabulatedComponent<H2O>;
    using CH4 = Dumux::Components::CH4<Scalar>;

    // turn components into gas/liquid phase
    using LiquidPhase = Dumux::FluidSystems::OnePLiquid<Scalar, TabH2O>;
    using GasPhase = Dumux::FluidSystems::OnePGas<Scalar, CH4>;
public:
    using type = Dumux::FluidSystems::TwoPImmiscible<Scalar, LiquidPhase, GasPhase>;
};
} // end namespace Properties

/*!
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,
        contiH2OEqIdx = Indices::conti0EqIdx
    };

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , time_(0.0)
    , initialShaleSaturation_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialShaleMethaneSaturation"))
    , injectionRate_(getParam<Scalar>("Problem.InjectionRate"))
    , injectionPosition_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionPosition"))
    , injectionLength_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionLength"))
    , injectionDuration_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionDuration"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::init();
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        values.setAllNeumann();

        // use Dirichlet at the top boundary
        if (globalPos[1] > this->gridGeometry().bBoxMax()[1] - 1e-6)
            values.setAllDirichlet();

        return values;
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated.
        values.setAllNeumann();
        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        auto values = initialAtPos(globalPos);

        // use zero-saturation within the overburden
        if (globalPos[1] > 35.0)
            values[saturationIdx] = 0.0;

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        auto values = NumEqVector(0.0);

        // within the injection region
        if (globalPos[1] < 1e-6
            && globalPos[0] > injectionPosition_
            && globalPos[0] < injectionPosition_ + injectionLength_
            && time_ < injectionDuration_ + 1e-6)
            values[contiH2OEqIdx] = injectionRate_;

        return values;
    }


    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // For the grid used here, the height of the domain is equal
        // to the maximum y-coordinate
        const auto domainHeight = this->gridGeometry().bBoxMax()[1];

        // we assume a constant water density of 1000 for initial conditions!
        const auto& g = this->spatialParams().gravity(globalPos);
        PrimaryVariables values;
        Scalar densityW = 1000.0;
        values[pressureIdx] = 1e5 - (domainHeight - globalPos[1])*densityW*g[1];

        // in the overburden (and maybe in the upper meters of the shale there is no gas initially)
        if (globalPos[1] > 35.0)  // take 35 meters if you only want zero gas in the overburden
            values[saturationIdx] = 0.0;

        // the shale formation contains gas initially
        else
            values[saturationIdx] = initialShaleSaturation_;

        return values;
    }

    //! computes the gas flux from the reservoir into the overburden
    template<class GridVariables, class SolutionVector, class Assembler, std::size_t id>
    Scalar computeGasFluxToOverburden(const GridVariables& gridVars,
                                      const SolutionVector& sol,
                                      const Assembler& assembler,
                                      const Dune::index_constant<id> domainId) const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            // only proceed if elem could potentially be on the interface to overburden
            const auto geometry = element.geometry();
            if (geometry.center()[1] < 35.0 && geometry.center()[1] > 15.0)
            {
                couplingManagerPtr_->bindCouplingContext(domainId, element, assembler);

                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(gridVars.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());
                elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

                for (const auto& scvf : scvfs(fvGeometry))
                {
                    if (scvf.boundary())
                        continue;

                    const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                    const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());

                    const bool isOnInterface = insideScv.center()[1] < 35.0 && outsideScv.center()[1] > 35.0;
                    if (isOnInterface)
                    {
                        using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
                        FluxVariables fluxVars;
                        fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                        // mass upwind term
                        const auto gasPhaseIdx = GetPropType<TypeTag, Properties::FluidSystem>::comp1Idx;
                        auto upwindTerm = [gasPhaseIdx] (const auto& volVars)
                        { return volVars.mobility(gasPhaseIdx)*volVars.density(gasPhaseIdx); };

                        flux += fluxVars.advectiveFlux(gasPhaseIdx, upwindTerm);
                    }
                }
            }
        }

        return flux;
    }

    //! computes the influx of water into the domain
    Scalar computeInjectionFlux() const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            for (const auto& scvf : scvfs(fvGeometry))
                if (scvf.boundary())
                    flux -= neumannAtPos(scvf.ipGlobal())[contiH2OEqIdx]*scvf.area();
        }

        return flux;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! sets the current time
    void setTime(Scalar time)
    { time_ = time; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar time_;
    Scalar initialShaleSaturation_;

    // injection specifics
    Scalar injectionRate_;
    Scalar injectionPosition_;
    Scalar injectionLength_;
    Scalar injectionDuration_;
};

} // end namespace Dumux

#endif
