// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
  */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH

// we use alu grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>

// we want to simulate methan gas transport in a water-saturated medium
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>

// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cctpfa.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class FractureSubProblem;

namespace Properties {
// template<class TypeTag, class MyTypeTag>
// struct GridData { using type = UndefinedProperty; }; // forward declaration of property

// create the type tag nodes
// Create new type tags
namespace TTag {
struct FractureProblemTypeTag { using InheritsFrom = std::tuple<TwoP, CCTpfaModel>; };
} // end namespace TTag
// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::FractureProblemTypeTag> { using type = Dune::FoamGrid<1, 2>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::FractureProblemTypeTag> { using type = FractureSubProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::FractureProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FractureSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FractureProblemTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2O = Dumux::Components::H2O<Scalar>;
    using TabH2O = Dumux::Components::TabulatedComponent<H2O>;
    using CH4 = Dumux::Components::CH4<Scalar>;

    // turn components into gas/liquid phase
    using LiquidPhase = Dumux::FluidSystems::OnePLiquid<Scalar, TabH2O>;
    using GasPhase = Dumux::FluidSystems::OnePGas<Scalar, CH4>;
public:
    using type = Dumux::FluidSystems::TwoPImmiscible<Scalar, LiquidPhase, GasPhase>;
};
} // end namespace Properties

/*!
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,
        contiH2OEqIdx = Indices::conti0EqIdx
    };

public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                       std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                       const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , time_(0.0)
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))
    , initialShaleSaturation_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialShaleMethaneSaturation"))
    , injectionRate_(getParam<Scalar>("Problem.InjectionRate"))
    , injectionPosition_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionPosition"))
    , injectionLength_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionLength"))
    , injectionDuration_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionDuration"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::init();
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // We only use no-flow boundary conditions for all immersed fractures
        // in the domain (fracture tips that do not touch the domain boundary)
        // Otherwise, we would lose mass leaving across the fracture tips.
        values.setAllNeumann();

        return values;
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    {
        // We treat the fractures as lower-dimensional in the grid,
        // but we have to give it the aperture as extrusion factor
        // such that the dimensions are correct in the end.
        return aperture_;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! evaluates the Neumann boundary condition for a given position
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    {
        auto values = NumEqVector(0.0);

        // within the injection region
        if (globalPos[1] < 1e-6
            && globalPos[0] > injectionPosition_
            && globalPos[0] < injectionPosition_ + injectionLength_
            && time_ < injectionDuration_ + 1e-6)
            values[contiH2OEqIdx] = injectionRate_;

        return values;
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // For the grid used here, the height of the domain is 52.5 m
        const auto domainHeight = 52.5;

        // we assume a constant water density of 1000 for initial conditions!
        const auto& g = this->spatialParams().gravity(globalPos);
        PrimaryVariables values;
        Scalar densityW = 1000.0;
        values[pressureIdx] = 1e5 - (domainHeight - globalPos[1])*densityW*g[1];
        values[saturationIdx] = initialShaleSaturation_;
        return values;
    }

    //! computes the influx of water into the domain
    Scalar computeInjectionFlux() const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            for (const auto& scvf : scvfs(fvGeometry))
                if (scvf.boundary())
                    flux -= neumannAtPos(scvf.ipGlobal())[contiH2OEqIdx]
                            *scvf.area()
                            *extrusionFactorAtPos(scvf.ipGlobal());
        }

        return flux;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! sets the current time
    void setTime(Scalar time)
    { time_ = time; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar time_;
    Scalar aperture_;
    Scalar initialShaleSaturation_;

    // injection specifics
    Scalar injectionRate_;
    Scalar injectionPosition_;
    Scalar injectionLength_;
    Scalar injectionDuration_;
};

} // end namespace Dumux

#endif
